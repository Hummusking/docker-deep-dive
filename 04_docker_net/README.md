
---
# Docker Networking
---

# Intro To Docker Networking 

Socket networking:

- Container Network Model (CNM)
    - Essentially OSI model for containers
    - Designed to outlines fundamental building blocks of docker network
- The *libnetwork*
    - Real life implementation fo CNM.
    - Used by docker to connect between containers.
    - Also used for service discovery, ingress (internal network) load balancing and network plane management functionality.

---

# Intro To Docker Networking (cont.)

### CNM Defines three building blocks:
<img src="99_misc/.img/endpoint.png" alt="drawing" style="float:right;width:400px;">

- `Sandboxes` : isolates network stack, meaning that containers do not have access to physical networking interfaces, routing tables, ports and dns.
- `Endpoints` : virtual network interfaces, responsible for connecting sandbox to a `network`
- `Networks` : software implementation of 802.1d bridge.

As described in diagram, each container has `Sandbox` component which provides networking connectivity for containers. Container A has single `Endpoint` that connects the container A to *Network A*. In the mean while, Container B has 2 `Endpoints`, one connecting to *Network A* and other connecting to *Network B*. Container A and B can communicate with each other, because they are on the same *Network A*, however, in container B, 2 existing Endpoints can not communicate, unless there is some additional layer 3 routing involved.

---

# Intro To Docker Networking (cont.)

*Libnetwork* uses drivers to extend the CNM model with specific network topologies, those drivers would be:

- **`bridge`** : The default network driver. If you don’t specify a driver, this is the type of network you are creating. Bridge networks are usually used when your applications run in standalone containers that need to communicate.
- **`host`** : For standalone containers, remove network isolation between the container and the Docker host, and use the host’s networking directly.
- **`overlay`** : Overlay networks connect multiple Docker daemons together and enable swarm services to communicate with each other. You can also use overlay networks to facilitate communication between a swarm service and a standalone container, or between two standalone containers on different Docker daemons. This strategy removes the need to do OS-level routing between these containers.

---

# Intro To Docker Networking (cont.)

- **`macvlan`** : Macvlan networks allow you to assign a MAC address to a container, making it appear as a physical device on your network. The Docker daemon routes traffic to containers by their MAC addresses. Using the `macvlan` driver is sometimes the best choice when dealing with legacy applications that expect to be directly connected to the physical network, rather than routed through the Docker host’s network stack.
- **`none`** : For this container, disable all networking. Usually used in conjunction with a custom network driver. none is not available for swarm services
- **`3rd party network plugins`** : You can install and use third-party network plugins with Docker. These plugins are available from Docker Hub or from third-party vendors. See the vendor’s documentation for installing and using a given network plugin.

Used to make containers communicate with each other

---

# Network driver summary

- User-defined `bridge` networks are best when you need multiple containers to communicate on the same Docker host.
- `Host` networks are best when the network stack should not be isolated from the Docker host, but you want other aspects of the container to be isolated.
- `Overlay` networks are best when you need containers running on different Docker hosts to communicate, or when multiple applications work together using swarm services.
- `Macvlan` networks are best when you are migrating from a VM setup or need your containers to look like physical hosts on your network, each with a unique MAC address.
- `Third-party network plugins` allow you to integrate Docker with specialized network stacks.

--- 

# Networking Commands

List networks

```sh
bash~$ docker network ls
```

Getting detailed network information

```sh
bash~$ docker network inspect br01
```

Create new network

```sh
bash~$ docker network create br01
```

Delete  network

```sh
bash~$ docker network rm br01
```

---
# Networking Commands (cont.)

### Bridge

Connect a container to a user-defined bridge

```sh
bash~$ docker create --name my-nginx --network br02 --publish 8080:80 nginx:latest
```

Connect pre-existing container to new network with `docker network connect <\NETWORK> <\CONTAINER>`

```sh
bash~$ docker container connect br02 my-nginx # assuming that my-nginx container was there before-hand
```
Disconnect a container from a user-defined bridge

```sh
bash~$ docker network disconnect br02 my-nginx
```

---
# Practice

- Start two alpine containers running shells with detached and interactive TTY's. provide them with 2 distinct names.
    - Connect to one of the alpine containers and try pinging :
        - Vaiolabs.io
        - Ip address of other container.
        - Host-name of other container.
- Create the alpine-net network with bridge driver.
    - Create two alpine containers that shall run on the alpine-net.
    - Create third alpine container that will NOT be part of alpine-net.
    - Try pinging alpine1 to alpine2 host-name from with in one of the containers.
    - Try ping IP address of alpine3 that is not in the same network.
    - Try pinging the host-name of alpine3.
    - Delete all containers and networks.
---
# Networking Commands (cont.)

### Host

If you use the host network mode for a container, that container’s network stack is not isolated from the Docker host (the container shares the host’s networking namespace), and the container does not get its own IP-address allocated. For instance, if you run a container which binds to port 80 and you use host networking, the container’s application is available on port 80 on the host’s IP address.

> `[!]` Note: Given that the container does not have its own IP-address when using host mode networking, port-mapping does not take effect, and the -p, --publish, -P, and --publish-all option are ignored, producing a warning instead.

Host mode networking can be useful to optimize performance, and in situations where a container needs to handle a large range of ports, as it does not require network address translation (NAT), and no proxy is created for each port.

`[!]` The host networking driver **`only works on Linux hosts`**, and **`is not supported`** on Docker Desktop for Mac, Docker Desktop for Windows, or Docker EE for Windows Server.

---

# Practice

- Create and start the nginx container as a detached process, yet also add `--rm` option.
    - Connect to it and check what ip does it has.
    - Validate that port 80 is accessible.
    - Try accessing from host the `localhost:80`

---

# Networking Commands (cont.)

### Overlay

The overlay network driver creates a distributed network among multiple Docker daemon hosts
This network sits on top of (overlays) the host-specific networks, allowing containers connected to it (including swarm service containers or k8s ) to communicate securely when encryption is enabled. Docker transparently handles routing of each packet to and from the correct Docker daemon host and the correct destination container

To create an overlay network for use with swarm services or k8s , use a command like the following:
```sh
bash~$ docker network create -d overlay my-overlay
```

To create an overlay network which can be used by swarm services or standalone containers to communicate with other standalone containers running on other Docker daemons, add the --attachable flag:

```sh
bash~$ docker network create -d overlay --attachable my-attachable-overlay
```

<!-- ---

# Practice
 will be practiced as part of k8s -->



---

# Networking Commands (cont.)

### MACVlan

Some applications, expect to be directly connected to the physical network. In this type of situation, you can use the macvlan network driver to assign a MAC address to each container’s virtual network interface, making it appear to be a physical network interface directly connected to the physical network. In this case, you need to designate a physical interface on your Docker host to use for the macvlan, as well as the subnet and gateway of the macvlan. You can even isolate your macvlan networks using different physical network interfaces. Keep the following things in mind:

- It is very easy to unintentionally damage your network due to IP address exhaustion or to “VLAN spread”, which is a situation in which you have an inappropriately large number of unique MAC addresses in your network.
- Your networking equipment needs to be able to handle “promiscuous mode”, where one physical interface can be assigned multiple MAC addresses.
- If your application can work using a bridge (on a single Docker host) or overlay (to communicate across multiple Docker hosts), these solutions may be better in the long term

---
# Networking Commands (cont.)

### MACVlan
### Create a macvlan network

When you create a macvlan network, it can either be in bridge mode or 802.1q trunk bridge mode.
- In bridge mode, macvlan traffic goes through a physical device on the host.
- In 802.1q trunk bridge mode, traffic goes through an 802.1q sub-interface which Docker creates on the fly. This allows you to control routing and filtering at a more granular level.

```sh
bash~$ docker network create -d macvlan --subnet=172.16.86.0/24 \
                          --gateway=172.16.86.1 -o parent=eth0 pub_net
```


---

# Networking Commands (cont.)

### MACVlan
### 802.1q trunk bridge mode

If you specify a parent interface name with a dot included, such as eth0.50, Docker interprets that as a sub-interface of eth0 and creates the sub-interface automatically.
```sh
bash~$ docker network create -d macvlan --subnet=192.168.50.0/24 \
                             --gateway=192.168.50.1 -o parent=eth0.50 macvlan50
```
---

# Practice

- Create docker network based on macvlan driver named local-macvlan-net.
- Start alpine container connected to local-macvlan-net network in interactive mode.
- Test network connection from the container.
- Inspect container details and check its network configuration.

---

# Networking Containers

Create a network with a subnet and gateway:

```sh
bash~$ docker network create --subnet 10.100.0.0/16 --gateway 10.100.100.1 br02
```

```sh
bash~$ docker network create --subnet 10.100.0.0/16\
      --gateway 10.100.100.1 --ip-range=10.100.4.0/24 br04
```

---

# Practice

- Create network inter-net
- Create container that is not connected to inter-net
- Connect previous container to inter-net
- Delete containers
- Delete network inter-net

---

# Networking Commands (cont.)

### Ports

Although not directly part of docker network, it has significant place in grand scheme of things. When running containers, we usually open connection to container, with exposing or publishing `port`.

- `--expose`: Expose a port or a range of ports
- `--publish` or `-p` : Publish a container's port(s) to the host

---

# Practice

- Create nginx container and expose port 8080 on host to port 80 inside the container.
- Create apache container exposed container ports 8081 and 8443 to ports 80 and 443 correspondingly.
- Create mysql container that has external and internal ports of 3306 exposed.
- Create redis container with internal 6378 port connected to external 66378 port.

---

# Summary Practice

- Create network with macvlan driver
  - Run `nginx` container on the external port 8080, 8443 and internal 80, 443 correspondigly. 
  - Connect to container and install openssl tool for creating self-signed ssl certificate. [example](https://stackoverflow.com/questions/10175812/how-to-generate-a-self-signed-ssl-certificate-using-openssl)
    - Create cert and place it in correct place.
    - Reload `nginx` without killing container.
  - Inspect the container details, and verify that IP is the same as your physical network.
    - If not, check your network driver and redo it.
  - Try accessing the container from different device (phone for example)
    - If you are not getting your self-signed cert, redo the exercise

